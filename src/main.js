import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Navbar, Button, Field, Input, Loading, Toast, Dialog } from "buefy";
import "@/assets/scss/app.scss";

Vue.use(Navbar);
Vue.use(Button);
Vue.use(Field);
Vue.use(Input);
Vue.use(Loading);
Vue.use(Toast);
Vue.use(Dialog);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app");
