import Vue from "vue";
import VueRouter from "vue-router";
import { isLogged } from "@/utils/user";
import store from "@/store";

const Home = () => import("@/views/Home.vue");
const Login = () => import("@/views/Login.vue");
const Register = () => import("@/views/Register.vue");

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/login",
    name: "Login",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Login,
    meta: {
      allowAnonymous: true
    }
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      allowAnonymous: true
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, _from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (isLogged()) {
      store.state.userStore.logged = true;
      next();
    } else {
      store.state.userStore.logged = false;
      next({ path: "/login" });
    }
  } else {
    if (to.matched.some((record) => record.meta.allowAnonymous) && isLogged()) {
      next({ path: "/" });
    } else {
      next();
    }
  }
});

export default router;
