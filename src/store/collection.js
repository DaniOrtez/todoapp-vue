import {
  getCollection,
  getCollectionsByUser,
  deleteCollection
} from "@/utils/collectionFetch";
import { newTask, deleteTask, editTask } from "@/utils/taskFetch";

const collection = {
  namespaced: true,
  state: () => ({
    collections: [],
    collectionsLoading: false,
    currentCollection: undefined,
    currentCollectionLoading: false
  }),
  mutations: {
    SET_COLLECTIONS(state, collections) {
      state.collections = collections;
    },
    SET_CURRENT_COLLECTION(state, collection) {
      state.currentCollection = collection;
    },
    ADD_COLLECTION(state, collection) {
      state.collections.push(collection);
    },
    setCurrentCollectionLoading(state, status) {
      state.currentCollectionLoading = status;
    },
    SET_COLLECTIONS_LOADING(state, status) {
      state.collectionsLoading = status;
    },
    DELETE_COLLECTION(state, collId) {
      state.collections = state.collections.filter((col) => col.id != collId);
    },
    CREATE_TASK(state, task) {
      state.currentCollection.tasks.push(task);
    },
    DELETE_TASK(state, id) {
      state.currentCollection.tasks = state.currentCollection.tasks.filter(
        (task) => task.id != id
      );
    },
    EDIT_TASK(state, data) {
      state.currentCollection.tasks = state.currentCollection.tasks.map(
        (task) => {
          if (task.id === data.id) task.done = data.done;
          return task;
        }
      );
    },
    CLEAR(state) {
      state.collections = [];
      state.currentCollection = undefined;
    }
  },
  actions: {
    async changeCollections({ commit }) {
      commit("SET_COLLECTIONS_LOADING", true);
      const collections = await getCollectionsByUser();
      commit("SET_COLLECTIONS_LOADING", false);
      return new Promise((resolve, reject) => {
        if (collections.code || collections.errors) {
          commit("SET_COLLECTIONS", []);
          reject(collections.message || collections.errors);
        } else {
          commit("SET_COLLECTIONS", collections);
          resolve();
        }
      });
    },
    async changeCurrentCollection({ commit }, id) {
      commit("setCurrentCollectionLoading", true);
      const collection = await getCollection(id);
      commit("SET_CURRENT_COLLECTION", collection);
      commit("setCurrentCollectionLoading", false);
    },
    addCollection({ commit }, collection) {
      const cleared = collection.collection;
      commit("ADD_COLLECTION", cleared);
    },
    async removeCollection({ state, commit }, id) {
      try {
        const deleted = await deleteCollection(id);
        if (deleted.errors) throw new Error();
        commit("DELETE_COLLECTION", id);
        if (state.currentCollection.id === id)
          commit("SET_CURRENT_COLLECTION", undefined);
      } catch (error) {
        return error;
      }
    },
    async createTask({ state, commit }, data) {
      const task = {
        collectionId: state.currentCollection.id,
        name: data,
        done: false
      };
      const created = await newTask(task);
      return new Promise((resolve, reject) => {
        if (created.errors || created.code)
          reject(created.message || created.errors);
        else {
          commit("CREATE_TASK", created.task);
          resolve();
        }
      });
    },
    async removeTask({ commit }, id) {
      const deleted = await deleteTask(id);
      return new Promise((resolve, reject) => {
        if (deleted.code || deleted.errors)
          reject(deleted.errors || deleted.message);
        else {
          commit("DELETE_TASK", id);
          resolve();
        }
      });
    },
    async toggleTask({ state, commit }, id) {
      let status;
      state.currentCollection.tasks.forEach((task) => {
        if (task.id === id) {
          status = task.done;
        }
      });
      const done = !status;
      const edited = await editTask(id, { done });
      return new Promise((resolve, reject) => {
        if (edited.code || edited.errors)
          reject(edited.errors || edited.message);
        else {
          commit("EDIT_TASK", { id, done });
          resolve();
        }
      });
    },
    clear({ commit }) {
      commit("CLEAR");
    }
  }
};

export default collection;
