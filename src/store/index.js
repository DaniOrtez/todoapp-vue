import Vue from "vue";
import Vuex from "vuex";
import userStore from "./user";
import collectionStore from "./collection";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { userStore, collectionStore }
});
