import { logIn, register } from "@/utils/user";

const user = {
  namespaced: true,
  state: () => ({
    user: "",
    logged: false
  }),
  mutations: {
    SET_AUTH(state, auth) {
      state.logged = auth;
    }
  },
  actions: {
    changeAuth({ commit }, auth) {
      commit("SET_AUTH", auth);
    },
    async login({ dispatch }, data) {
      const loggin = await logIn(data.email, data.password);
      return new Promise((resolve, reject) => {
        if (loggin.status !== 200) {
          dispatch("changeAuth", false);
          if (loggin.error || loggin.code) reject(loggin.error);
          else
            reject({ code: "server/error", message: "An error has ocurred" });
        } else {
          dispatch("changeAuth", true);
          resolve();
        }
      });
    },
    async register({ dispatch }, data) {
      const registered = await register(data);
      return new Promise((resolve, reject) => {
        if (registered.status !== 201) {
          dispatch("changeAuth", false);
          if (registered.error.error != undefined) {
            reject(registered.error);
          }
          reject();
        } else {
          dispatch("changeAuth", true);
          resolve();
        }
      });
    }
  }
};

export default user;
