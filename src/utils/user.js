import jwtDecode from "jwt-decode";
import store from "@/store";

const url = process.env.VUE_APP_API_URL;

export const localToken = (action, token = undefined) => {
  switch (action) {
    case "set":
      localStorage.setItem("authToken", token);
      break;
    case "get":
      return localStorage.getItem("authToken");
    case "delete":
      localStorage.removeItem("authToken");
      break;
    default:
      break;
  }
};

export const isLogged = () => {
  const regex = /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/;
  const token = localToken("get");
  let decoded;
  if (token) {
    decoded = jwtDecode(token);
    store.state.userStore.user = decoded.name;
  }
  const tokenVerified = () => {
    if (!token || !regex.test(token) || decoded.exp * 1000 < Date.now()) {
      return false;
    } else {
      return true;
    }
  };
  const result = tokenVerified();
  return result;
};

export async function logIn(email, password) {
  const data = {
    email,
    password
  };
  const options = {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  };
  try {
    const user = await fetch(`${url}/user/login`, options);
    const json = await user.json();
    if (user.status !== 200) {
      const error = {
        status: user.status,
        error: json.errors ? json.errors : json
      };
      throw error;
    }
    localToken("set", json.token);
    return {
      status: user.status
    };
  } catch (catched) {
    const { status, error } = catched;
    return { status, error };
  }
}

export async function register(data) {
  const options = {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  };
  try {
    const user = await fetch(`${url}/user/register`, options);
    const json = await user.json();
    if (user.status !== 201) {
      const error = {
        status: user.status,
        error: json.errors ? json.errors : json
      };
      throw error;
    }
    localToken("set", json.token);
    return {
      status: user.status
    };
  } catch (catched) {
    const { status, error } = catched;
    return {
      status,
      error
    };
  }
}
