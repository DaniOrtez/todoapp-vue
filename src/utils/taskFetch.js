const user = () => import("./user");

const url = process.env.VUE_APP_API_URL;

export async function newTask(task) {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(task)
  };
  try {
    const task = await fetch(`${url}/collections/task`, options);
    const result = task.json();
    return result;
  } catch (error) {
    return error;
  }
}

export async function deleteTask(id) {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "DELETE",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  };
  try {
    const deleted = await fetch(`${url}/collections/task/${id}`, options);
    const result = await deleted.json();
    return result;
  } catch (error) {
    return error;
  }
}

export async function editTask(id, data) {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "PATCH",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(data)
  };
  try {
    const edited = await fetch(`${url}/collections/task/${id}`, options);
    const result = await edited.json();
    return result;
  } catch (error) {
    return error;
  }
}
