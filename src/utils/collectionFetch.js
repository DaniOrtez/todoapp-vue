/* TESTING LAZY LOADING */
const user = () => import("./user");

const url = process.env.VUE_APP_API_URL;

export async function getCollectionsByUser() {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  };
  try {
    const collections = await fetch(`${url}/collections`, options);
    const collectionsJSON = await collections.json();
    return collectionsJSON.collections;
  } catch (error) {
    return error;
  }
}

export async function getCollection(id) {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  };
  try {
    const request = await fetch(`${url}/collections/${id}`, options);
    const collection = await request.json();
    return collection.collection;
  } catch (error) {
    return error;
  }
}

export async function createCollection(name) {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      name
    })
  };
  try {
    const request = await fetch(`${url}/collections`, options);
    const result = await request.json();
    return result;
  } catch (error) {
    return 400;
  }
}

export async function deleteCollection(collectionId) {
  const token = await user().then(({ localToken }) => localToken("get"));
  const options = {
    method: "DELETE",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      collectionId
    })
  };
  try {
    const request = await fetch(`${url}/collections`, options);
    const result = await request.json();
    return result;
  } catch (error) {
    return 400;
  }
}
