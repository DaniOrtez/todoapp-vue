import { required } from "vuelidate/lib/validators";

export const collectionMixin = {
  data() {
    return {
      name: ""
    };
  },
  validations: {
    name: { required }
  },
  methods: {
    toast(message, type, duration = 3000, position = "is-top") {
      return this.$buefy.toast.open({
        message,
        type,
        duration,
        position
      });
    }
  }
};
